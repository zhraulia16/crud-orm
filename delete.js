const {
    books
} = require('./models')

const query = {
where: {id: 4}
}
books.destroy(query)
.then(() => {
    console.log(`Buku dengan id 4 berhasil dihapus`)
    process.exit()
})
.catch(err => {
    console.error("Buku gagal dihapus")
})