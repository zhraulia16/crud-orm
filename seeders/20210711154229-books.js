'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('books', [{
      isbn: '345HA',
      judul: 'Hachiko',
      sinopsis: null,
      penulis: 'Zahra',
      genre: 'Drama',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      isbn: '346HA',
      judul: 'Habit',
      sinopsis: null,
      penulis: 'Charles Duhigg',
      genre: 'Non fiction',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      isbn: '347HA',
      judul: 'Pukat',
      sinopsis: null,
      penulis: 'Tere Liya',
      genre: 'Drama',
      createdAt: new Date(),
      updatedAt: new Date()
    }])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

     return queryInterface.bulkDelete('books', null, {})
  }
};